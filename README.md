# opa-policy
This project demonstrates a deployment of OPA Gatekeeper using the Carvel Packaging API and Kapp Controller to apply policy

### Install Gatekeeper

Using the Tanzu Community Package Repository, we have a variety of packages that can be easily deployed on any K8s cluster

```
NAMESPACE            NAME                                                   PACKAGEMETADATA NAME                            VERSION   AGE
package-management   cert-manager.community.tanzu.vmware.com.1.3.1          cert-manager.community.tanzu.vmware.com         1.3.1     3h19m37s
package-management   cert-manager.community.tanzu.vmware.com.1.4.0          cert-manager.community.tanzu.vmware.com         1.4.0     3h19m36s
package-management   contour-operator.community.tanzu.vmware.com.1.15.1     contour-operator.community.tanzu.vmware.com     1.15.1    3h19m36s
package-management   contour-operator.community.tanzu.vmware.com.1.16.0     contour-operator.community.tanzu.vmware.com     1.16.0    3h19m36s
package-management   contour.community.tanzu.vmware.com.1.15.1              contour.community.tanzu.vmware.com              1.15.1    3h19m36s
package-management   contour.community.tanzu.vmware.com.1.17.1              contour.community.tanzu.vmware.com              1.17.1    3h19m36s
package-management   external-dns.community.tanzu.vmware.com.0.8.0          external-dns.community.tanzu.vmware.com         0.8.0     3h19m36s
package-management   fluent-bit.community.tanzu.vmware.com.1.7.5            fluent-bit.community.tanzu.vmware.com           1.7.5     3h19m36s
package-management   gatekeeper.community.tanzu.vmware.com.1.0.0            gatekeeper.community.tanzu.vmware.com           1.0.0     3h19m37s
package-management   grafana.community.tanzu.vmware.com.7.5.7               grafana.community.tanzu.vmware.com              7.5.7     3h19m37s
package-management   harbor.community.tanzu.vmware.com.2.2.3                harbor.community.tanzu.vmware.com               2.2.3     3h19m37s
package-management   knative-serving.community.tanzu.vmware.com.0.22.0      knative-serving.community.tanzu.vmware.com      0.22.0    3h19m37s
package-management   local-path-storage.community.tanzu.vmware.com.0.0.19   local-path-storage.community.tanzu.vmware.com   0.0.19    3h19m37s
package-management   multus-cni.community.tanzu.vmware.com.3.7.1            multus-cni.community.tanzu.vmware.com           3.7.1     3h19m36s
package-management   prometheus.community.tanzu.vmware.com.2.27.0           prometheus.community.tanzu.vmware.com           2.27.0    3h19m36s
package-management   spring-music.pkg.demo.1.1.0                            spring-music.pkg.demo                           1.1.0     3h20m6s
package-management   velero.community.tanzu.vmware.com.1.5.2                velero.community.tanzu.vmware.com               1.5.2     3h19m37s
package-management   vendor-app.pkg.demo.1.1.0                              vendor-app.pkg.demo                             1.1.0     3h20m6s
```

Using the Tanzu Package Repository, we deploy gatekeepr as a package install

```yaml
---
apiVersion: packaging.carvel.dev/v1alpha1
kind: PackageInstall
metadata:
  name: gatekeeper
  namespace: package-management
spec:
  serviceAccountName: pkg-sa
  packageRef:
    refName: gatekeeper.community.tanzu.vmware.com
    versionSelection:
      constraints: 1.0.0 

```

Now we have gatekeeper running in our cluster

```
tseufert-a01:opa-policy tseufert$ kubectl get all -n gatekeeper-system
NAME                                                 READY   STATUS    RESTARTS   AGE
pod/gatekeeper-audit-9f844b5b9-fp57t                 1/1     Running   0          150m
pod/gatekeeper-controller-manager-5d67c74dd9-2m7lx   1/1     Running   0          150m

NAME                                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
service/gatekeeper-webhook-service   ClusterIP   100.64.34.104   <none>        443/TCP   150m

NAME                                            READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/gatekeeper-audit                1/1     1            1           150m
deployment.apps/gatekeeper-controller-manager   1/1     1            1           150m

NAME                                                       DESIRED   CURRENT   READY   AGE
replicaset.apps/gatekeeper-audit-9f844b5b9                 1         1         1       150m
replicaset.apps/gatekeeper-controller-manager-5d67c74dd9   1         1         1       150m
```

### Apply Policy

Now to go about applying policy from our git repository.  this project has a policy folder with a couple of example constraints pulled from https://github.com/open-policy-agent/gatekeeper-library 

Gatekeeper generates CRDs for each ContstraintTemplate, this is currently an issue for Kapp Controller because CRDs do not exist before the deployment begins.  https://github.com/vmware-tanzu/carvel-kapp/issues/52

For now we will just apply the policy directory

```
##may need to run twice because of CRD creation
kubectl apply -f policy/

```

### Test policy

Lets test the policy

```
kubectl apply -f test/

```

With the following output

```
Error from server ([denied by psp-host-filesystem] HostPath volume {"name": "cache-volume", "hostPath": {"path": "/tmp", "type": ""}} is not allowed, pod: nginx-host-filesystem. Allowed path: [{"pathPrefix": "/foo", "readOnly": true}]): error when creating "test/host-path-fail.yaml": admission webhook "validation.gatekeeper.sh" denied the request: [denied by psp-host-filesystem] HostPath volume {"name": "cache-volume", "hostPath": {"path": "/tmp", "type": ""}} is not allowed, pod: nginx-host-filesystem. Allowed path: [{"pathPrefix": "/foo", "readOnly": true}]

Error from server ([denied by psp-privileged-container] Privileged container is not allowed: nginx, securityContext: {"privileged": true}): error when creating "test/priv-pod-fail.yaml": admission webhook "validation.gatekeeper.sh" denied the request: [denied by psp-privileged-container] Privileged container is not allowed: nginx, securityContext: {"privileged": true}

```
